<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMovies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->uuid('m_title_cast1')->index()->nullable();
            $table->uuid('m_title_cast2')->index()->nullable();  
            $table->uuid('m_cast1p')->index()->nullable();
            $table->uuid('m_cast2p')->index()->nullable();             
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
