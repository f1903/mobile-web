
# Project Title

Build on :
- Laravel framework 8 using REST API with API Authenticated
- The Front-end is VueJS (Single Page Application)


# Server Requirements

- PHP >= 7.3
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- PHP GD Extension
- NodeJS (+12) + Npm
- Libpng-dev (pngquant)
- Apache / Nginx
- Mysql 5.7.*
- Php-fpm


# Laravel Configuration

To config laravel, run the following command

```bash
  php artisan config:cache // Clear cache after edit to .env
  php artisan migrate  // Migrate DB
  php artisan db:seed  // Create admin user
  php artisan storage:link // create symlink for storage folder
```

Create oAuth secret ID, Please after run this command copy secret ID 2 and add it in note

```bash
  php artisan passport:install
```
Add client secret ID 2 to this file

```bash
  /resources/assets/js/users/packages/Helper.js
```
# Run Locally

Install Vendor etc.

```bash
  npm install
  npm install -g sass-migrator
  sass-migrator division **/*.scss
```

Watch

```bash
  npm watch
```

Start the server

```bash
  npm run prod
```
# Database
Import database from

```bash
    ~/prod.sql
```