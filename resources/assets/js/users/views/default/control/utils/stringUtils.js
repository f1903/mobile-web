// https://gist.github.com/cvergne/7244472d3e1f1ca7ee56

String.prototype.getInitials = function(glue){
    if (typeof glue == "undefined") {
        var glue = true;
    }
  
    var initials = this.replace(/[^a-zA-Z- ]/g, "").match(/\b\w/g);
    
    if (glue) {
        return initials.join('');
    }
  
    return  initials;
  };
  
  String.prototype.capitalize = function(){
    return this.toLowerCase().replace( /\b\w/g, function (m) {
        return m.toUpperCase();
    });
  };
  
  
  /* Thanks to https://stackoverflow.com/questions/54651201/how-do-i-covert-kebab-case-into-pascalcase */
  String.prototype.toCamelCase = function(){
    return this.replace(/-\w/g, clearAndUpper);
  }
  
  String.prototype.toPascalCase = function(){
    return this.replace(/(^\w|-\w)/g, clearAndUpper);
  };
  
  function clearAndUpper(text) {
    return text.replace(/-/, "").toUpperCase();
  }