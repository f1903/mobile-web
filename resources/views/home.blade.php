<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta name="theme-color" content="#5cbda5">
    <meta name="description" content="">
    <link rel="icon" sizes="192x192" href="/favicon.ico">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <script src="https://cdn.jwplayer.com/libraries/CEdQwcUC.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-194346711-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-194346711-1');
    </script>

</head>
<body id="root">
<!--  container content: START -->
<div class="{{config('plugin.theme')}}"></div>
<!--  container content: END -->
<noscript>

    <div class="container">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="alert alert-danger">
                JavaScript is disabled in your web browser!
            </div>
        </div>
    </div>
</noscript>  <!--  container javascript alert: END -->
<script src="/js/admin-js/manifest.js"></script>
<script src="/js/admin-js/vendor.js"></script>

<script src="{{ (env('APP_ENV') === 'development') ? mix('js/app.js') : asset('js/app.js') }}"></script>
</body>
</html>
